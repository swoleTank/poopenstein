#ifndef SDLGAMEWINDOW_H
#define SDLGAMEWINDOW_H

#include <string>

#include "GameWindow.h"

class SDL_Window;
class SDL_Renderer;

class SDLGameWindow : public GameWindow
{
public:
	SDLGameWindow();
	~SDLGameWindow();
	bool init() override;
	void run() override;
	void drawPoint(int x, int y, Color color) override;
	virtual void userUpdate() = 0;

	void setTitle(const std::string& title) override;
	void setWinSize(int width, int height) override;

	int m_width, m_height;
	bool m_gameLoop;

	float deltaTime;

protected:
    void setInput() override;

private:
	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;
};

#endif
