#ifndef NCURSESGAMEWINDOW_H
#define NCURSESGAMEWINDOW_H

#include <string>
#include <ncurses.h>
#include <vector>

#include "GameWindow.h"

typedef struct _HSB
{
    double h,s,b;
} HSB;

typedef struct _COLORDIST
{
    HSB hsb;
    int itr;
} ColorDist;


class NCursesGameWindow : public GameWindow
{
public:
	NCursesGameWindow();
	~NCursesGameWindow();
	bool init() override;
	void run() override;
	void drawPoint(int x, int y, Color color) override;
	virtual void userUpdate() = 0;

	void setTitle(const std::string& title) override;
	void setWinSize(int width, int height) override;

	int m_width, m_height;
	bool m_gameLoop;

	float deltaTime;

protected:
    void setInput() override;

    void genColorPairs();
    int findColorPair(Color color);
    HSB RGBtoHSB(int red, int green, int blue);
    int GetNearestColorLabelIndex(Color color);

    std::vector<ColorDist> m_vColorDists;
    
    WINDOW* m_win;
};

#endif
