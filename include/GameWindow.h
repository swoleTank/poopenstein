#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <string>

class SDL_Window;
class SDL_Renderer;

typedef struct _Color
{
	int r, g, b, a;
} Color;

typedef struct _Input
{
    bool UP;
    bool DOWN;
    bool STRAFE_LEFT;
    bool STRAFE_RIGHT;
    bool LOOK_LEFT;
    bool LOOK_RIGHT;
} Input;

class GameWindow
{
public:
	virtual bool init() = 0;
	virtual void run() = 0;
	virtual void drawPoint(int x, int y, Color color) = 0;
	virtual void userUpdate() = 0;

	virtual void setTitle(const std::string& title) = 0;
	virtual void setWinSize(int width, int height) = 0;

	int m_width, m_height;
	bool m_gameLoop;

	float deltaTime;
protected:
    virtual void setInput() = 0;

    Input m_input;
	std::string m_title;
	uint64_t m_lastFrame;
	uint64_t m_currentFrame;
};

#endif
