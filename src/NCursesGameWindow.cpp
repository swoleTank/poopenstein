#include <iostream>
#include <unistd.h>
#include <cmath>
#include <algorithm>
#include <limits>

#include "NCursesGameWindow.h"

NCursesGameWindow::NCursesGameWindow()
{
	m_title="basic title";
    m_win = initscr();
    start_color();
    genColorPairs();
    getmaxyx(m_win, m_height, m_width);
    keypad(stdscr, TRUE);
    cbreak();
    noecho();
    clear();

	m_gameLoop = false;
	
	deltaTime = 0.0f;
	m_lastFrame = 0;
	m_currentFrame = 0;
}

NCursesGameWindow::~NCursesGameWindow()
{
    endwin();
}

bool NCursesGameWindow::init()
{
	return true;
}

void NCursesGameWindow::drawPoint(int x, int y, Color color)
{
    //int colorThing = 2;
    int colorThing = findColorPair(color);
    //init_pair(colorThing, COLOR_RED, COLOR_RED);
    attron(COLOR_PAIR(colorThing));
    mvaddch(y, x, ' ');
    attroff(COLOR_PAIR(colorThing));
  
  /*
    for(int j = 0; j < COLORS-1; j++)
    {
        //init_pair(j, j, j);
        attron(COLOR_PAIR(j));
        mvaddch(0, j, ' ');
        attroff(COLOR_PAIR(j));
    }
    */
    //printw("My terminal supports %d colors.\n", COLORS);
    //printw("My terminal supports %d colors.\n", m_vColorDists.size());
}

void NCursesGameWindow::setTitle(const std::string& title)
{
	m_title = title;
}

void NCursesGameWindow::setWinSize(int width, int height)
{
	m_width = width;
	m_height = height;
}

void NCursesGameWindow::run()
{

	m_gameLoop = true;	
	//m_lastFrame = SDL_GetPerformanceCounter();
	while(m_gameLoop)
	{
        setInput();
		//m_currentFrame = SDL_GetPerformanceCounter();
		//uint64_t framesElapsed = m_currentFrame - m_lastFrame;
		//deltaTime = (float)(framesElapsed / 1000000000.0f);
		//m_lastFrame = m_currentFrame;
        deltaTime = 0.1;

        refresh();

		userUpdate();
	}
}

void NCursesGameWindow::setInput()
{
    m_input = {false, false, false, false, false, false};
    int ch = getch();
    if(ch == 'w' || ch == 'W')
        m_input.UP = true;
    if(ch == 's' || ch == 'S')
        m_input.DOWN = true;
    if(ch == KEY_LEFT)
        m_input.LOOK_LEFT = true;
    if(ch == KEY_RIGHT)
        m_input.LOOK_RIGHT = true;
    if(ch == 'a' || ch == 'A')
        m_input.STRAFE_LEFT = true;
    if(ch == 'd' || ch == 'D')
        m_input.STRAFE_RIGHT = true;
    if (ch == 'q' || ch == 'Q')
        m_gameLoop = false;	
}

void NCursesGameWindow::genColorPairs()
{
    // set first to black and last to white
    init_color(0, 0, 0, 0);
    init_color(COLORS-1, 1000, 1000, 1000);
    
    int itr = 1;
    for (double i = 1; i < COLORS-1; i++)
    {
        double ratio = i/(double)COLORS;
        //we want to normalize ratio so that it fits in to 6 regions
        //where each region is 256 units long
        int normalized = int(ratio * 256 * 6);

        //find the distance to the start of the closest region
        int x = normalized % 256;

        int red = 0, grn = 0, blu = 0;
        switch(normalized / 256)
        {
        case 0: red = 255;      grn = x;        blu = 0;       break;//red
        case 1: red = 255 - x;  grn = 255;      blu = 0;       break;//yellow
        case 2: red = 0;        grn = 255;      blu = x;       break;//green
        case 3: red = 0;        grn = 255 - x;  blu = 255;     break;//cyan
        case 4: red = x;        grn = 0;        blu = 255;     break;//blue
        case 5: red = 255;      grn = 0;        blu = 255 - x; break;//magenta
        }
        
        HSB hsb = RGBtoHSB(red,grn,blu);
        ColorDist cVal{hsb, itr};
        m_vColorDists.push_back(cVal);
        
        
        
        red = (red*1000)/255;
        grn = (grn*1000)/255;
        blu = (blu*1000)/255;

        init_color(itr, red, grn, blu);
        init_pair(itr, itr, itr);
        
        refresh();
        itr++;   
    }
}

/// <summary>
/// Converts RGB to HSB from http://www.codeproject.com/cs/algorithms/colorspace1.asp
/// </summary>

/*public static*/ HSB NCursesGameWindow::RGBtoHSB(int red, int green, int blue)
{
    // normalize red, green and blue values
    double r = ((double)red / 255.0);
    double g = ((double)green / 255.0);
    double b = ((double)blue / 255.0);
    // conversion start
    double max = std::max(r, std::max(g, b));
    double min = std::min(r, std::min(g, b));
    double h = 0.0;
    if (max == r && g >= b)
    {
        h = 60 * (g - b) / (max - min);
    }
    else if (max == r && g < b)
    {
        h = 60 * (g - b) / (max - min) + 360;
    }
    else if (max == g)
    {
        h = 60 * (b - r) / (max - min) + 120;
    }
    else if (max == b)
    {
        h = 60 * (r - g) / (max - min) + 240;
    }
    double s = (max == 0) ? 0.0 : (1.0 - (min / max));
    HSB hsb{h,s,(double)max};
    return hsb;
}

/// <summary>
/// Returns the nearest matching label color index available from the Label color list for a given color
/// </summary>
/// <param name="color">Whose match is to be found</param>
/// <returns>Label Color Index</returns>

/*public static*/ int NCursesGameWindow::GetNearestColorLabelIndex(Color color)
{
    // adjust these values to place more or less importance on
    // the differences between HSV components of the colors
    const double weightHue = 0.8;
    const double weightSaturation = 0.1;
    const double weightValue = 0.1;
    double minDistance = std::numeric_limits<double>::max();
    int minIndex = 0;
    HSB targetHSB = RGBtoHSB(color.r, color.g, color.b);
    int i = 0;
    for (auto const & colorDist : m_vColorDists)
    {
        double dH = colorDist.hsb.h - targetHSB.h;
        double dS = colorDist.hsb.s - targetHSB.s;
        double dV = colorDist.hsb.b - targetHSB.b;
        
        double curDistance = sqrt(weightHue * pow(dH, 2) + weightSaturation * pow(dS, 2) + weightValue * pow(dV, 2));
        
        if (curDistance < minDistance)
        {
            minDistance = curDistance;
            minIndex = i;
        }
        i++;
    }
    return m_vColorDists[minIndex].itr;
}


int NCursesGameWindow::findColorPair(Color color)
{
    int test =  GetNearestColorLabelIndex(color);
    return test;
}

