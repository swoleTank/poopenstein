#include <iostream>
#include <SDL2/SDL.h>

#include "SDLGameWindow.h"

SDLGameWindow::SDLGameWindow()
{
	m_pWindow = NULL;
	m_pRenderer = NULL;

	m_title="basic title";
	m_width = 640;
	m_height = 480;
	m_gameLoop = false;
	
	deltaTime = 0.0f;
	m_lastFrame = 0;
	m_currentFrame = 0;
}

SDLGameWindow::~SDLGameWindow()
{
	if(m_pWindow)
		SDL_DestroyWindow(m_pWindow);

	if(m_pRenderer)
		SDL_DestroyRenderer(m_pRenderer);

	SDL_Quit();
}

bool SDLGameWindow::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
    {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return false;
    }
    
	m_pWindow = SDL_CreateWindow(
			m_title.c_str(),
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			m_width,
			m_height,
			0
	);

	if(m_pWindow == NULL)
	{
		std::cout << "Could not create window: " << SDL_GetError() << std::endl;
		return false;
	}

	m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED);

	if(m_pRenderer == NULL)
	{
		std::cout << "Could not create renderer: " << SDL_GetError() << std::endl;
		return false;
	}

	return true;
}

void SDLGameWindow::drawPoint(int x, int y, Color color)
{
	SDL_SetRenderDrawColor(m_pRenderer, color.r, color.g, color.b, color.a);
	SDL_RenderDrawPoint(m_pRenderer, x, y);
}

void SDLGameWindow::setTitle(const std::string& title)
{
	m_title = title;
	if(m_pWindow)
	{
		SDL_SetWindowTitle(m_pWindow, title.c_str());
	}
}

void SDLGameWindow::setWinSize(int width, int height)
{
	m_width = width;
	m_height = height;
	if(m_pWindow)
		SDL_SetWindowSize(m_pWindow, m_width, m_height);
}

void SDLGameWindow::run()
{

	m_gameLoop = true;	
	m_lastFrame = SDL_GetPerformanceCounter();
	while(m_gameLoop)
	{
        setInput();
		m_currentFrame = SDL_GetPerformanceCounter();
		uint64_t framesElapsed = m_currentFrame - m_lastFrame;
		deltaTime = (float)(framesElapsed / 1000000000.0f);
		m_lastFrame = m_currentFrame;

		SDL_RenderClear(m_pRenderer);

		userUpdate();

		SDL_RenderPresent(m_pRenderer);
	}
}

void SDLGameWindow::setInput()
{
    m_input = {false, false, false, false, false, false};
    const unsigned char* keystates = SDL_GetKeyboardState(NULL);
    if(keystates[SDL_SCANCODE_W])
        m_input.UP = true;
    if(keystates[SDL_SCANCODE_S])
        m_input.DOWN = true;
    if(keystates[SDL_SCANCODE_LEFT])
        m_input.LOOK_LEFT = true;
    if(keystates[SDL_SCANCODE_RIGHT])
        m_input.LOOK_RIGHT = true;
    if(keystates[SDL_SCANCODE_A])
        m_input.STRAFE_LEFT = true;
    if(keystates[SDL_SCANCODE_D])
        m_input.STRAFE_RIGHT = true;

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            m_gameLoop = false;	
        }
    }
}
