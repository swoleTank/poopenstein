#include <SDL2/SDL.h>
#include <iostream>
#include <cmath>

#include "SDLGameWindow.h"
#include "NCursesGameWindow.h"

typedef struct _Player
{
	_Player()
	{
		x = 8.0f;
		y = 8.0f;
		a = 0.0f;
		fFov = 3.141592654f / 4.0f;
	}
	float x, y, a;	
	float fFov;
} Player;

class FPSGame : public NCursesGameWindow
{
public:
	FPSGame()
	{
		nMapWidth = 16;
		nMapHeight = 16;

		map += L"################";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#.........#....#";
		map += L"#.........#....#";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#......#########";
		map += L"#..............#";
		map += L"#..............#";
		map += L"#..............#";
		map += L"################";

		m_fDepth = 16.0f;
	}

	virtual void userUpdate()
	{
	    if(m_input.UP)
	    {
			m_player.x += sinf(m_player.a) * 5.0 * deltaTime;
			m_player.y += cosf(m_player.a) * 5.0 * deltaTime;
	    }
	    if(m_input.DOWN)
	    {
			m_player.x -= sinf(m_player.a) * 5.0 * deltaTime;
			m_player.y -= cosf(m_player.a) * 5.0 * deltaTime;
	    }
	    if(m_input.LOOK_LEFT)
	    {
			m_player.a -= (0.9f) * deltaTime;
	    }
	    if(m_input.LOOK_RIGHT)
	    {
			m_player.a += (0.9f) * deltaTime;
	    }
	    if(m_input.STRAFE_LEFT)
	    {
			m_player.x -= cosf(m_player.a) * 5.0 * deltaTime;
			m_player.y += sinf(m_player.a) * 5.0 * deltaTime;
	    }
	    if(m_input.STRAFE_RIGHT)
	    {
			m_player.x += cosf(m_player.a) * 5.0 * deltaTime;
			m_player.y -= sinf(m_player.a) * 5.0 * deltaTime;
	    }

		for(int x = 0; x < m_width; x++)
		{
			// for each pixel, calculate the project angle into world space
			float fRayAngle = (m_player.a - m_player.fFov / 2.0f) + ((float) x / (float)m_width) * m_player.fFov;

			float fDistanceToWall = 0.0f;
			bool bHitWall = false;

			float fEyeX = sinf(fRayAngle); // Unit vector for ray in player space
			float fEyeY = cosf(fRayAngle);
			
			while(!bHitWall && fDistanceToWall < m_fDepth)
			{
				fDistanceToWall += 0.1f;

				int nTestX = (int)(m_player.x + fEyeX * fDistanceToWall);
				int nTestY = (int)(m_player.y + fEyeY * fDistanceToWall);

				// test if ray is out of bounds
				if(nTestX < 0 || nTestX >= nMapWidth || nTestY < 0 || nTestY >= nMapHeight)
				{
					bHitWall = true;
					fDistanceToWall = m_fDepth;
				}
				else
				{
					// Ray is inbounds so test to see if the ray cell is a wall block
					if(map[nTestY * nMapHeight + nTestX] == '#')
					{
						bHitWall = true;
					}
				}
			}

			// Calculate distance to ceiling and floor
			int nCeiling = (float)(m_height / 2.0) - m_height / ((float) fDistanceToWall);
			int nFloor = m_height - nCeiling;

			int nShade = abs(((fDistanceToWall / m_fDepth) * 255)- 255);

			/*
			int nShade = 0;
			if(fDistanceToWall <= m_fDepth / 4.0f)			nShade = 255;
			else if (fDistanceToWall < m_fDepth / 3.0f)		nShade = 255 * 0.75;
			else if (fDistanceToWall < m_fDepth / 2.0f)		nShade = 255 * 0.50;
			else if (fDistanceToWall < m_fDepth)				nShade = 255 * 0.25;
			else											nShade = 25;
			*/

			for(int y = 0; y < m_height; y++)
			{
				float b = abs(((1.0f - (((float)y - m_height / 2.0f) / ((float)m_height / 2.0f))) * 255.0f) - 255.0f);
				Color drawColor;
				if(y < nCeiling)
				{
					drawColor.r = int(b/4.0f);
					drawColor.g = 0;
					drawColor.b = int(b/2.0f);
					drawColor.a = 255;
				}
				else if(y > nCeiling && y <= nFloor)
				{
					drawColor.r = nShade;
					drawColor.g = nShade;
					drawColor.b = nShade;
					drawColor.a = 255;
				}
				else
				{

					drawColor.r = 0;
					drawColor.g = int(b/4.0f);
					drawColor.b = int(b);
					drawColor.a = 255;
				}
				drawPoint(x, y, drawColor);
			}
		}
	}

private:
	Player m_player;

	int nMapWidth, nMapHeight;
	std::wstring map;
	float m_fDepth;
};	

int main(int argc, char** argv)
{	
	FPSGame game;
	game.setTitle("Poopenstein 3D");
	game.init();
	game.run();
	return 0;
}
