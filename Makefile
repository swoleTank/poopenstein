SOURCEDIR = src
BUILDDIR = build

CXXFLAGS = -std=c++17 -Wall -g -Iinclude 
LDFLAGS = -lSDL2 -lm -lncurses
TARGET = poopenstein 
SRCS = $(wildcard $(SOURCEDIR)/*.cpp)
OBJS = $(patsubst $(SOURCEDIR)/%.cpp,$(BUILDDIR)/%.o,$(SRCS))

all: $(TARGET)\

$(TARGET): $(OBJS) 
	$(CXX) -o $(TARGET) $(CXXFLAGS) $(OBJS) $(LDFLAGS)
	
$(OBJS): $(BUILDDIR)/%.o : $(SOURCEDIR)/%.cpp
	$(CXX) -c $(CXXFLAGS) -DBOOST_LOG_DYN_LINK $< -o $@

.PHONY: test clean

test: EngineTest
	echo "No tests. yet ;)"

clean:
	rm -f $(BUILDDIR)/* core *.core $(TARGET)
	
install:
	echo "Installing is not supported"
	
run:
	./$(TARGET)
